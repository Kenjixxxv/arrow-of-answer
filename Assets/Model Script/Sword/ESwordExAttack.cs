﻿using UnityEngine;
using System.Collections;

public class ESwordExAttack : MonoBehaviour {

    Animator animator;
    // Use this for initialization
    void Start()
    {
        animator = GetComponent(typeof(Animator)) as Animator;
    }

    // Update is called once per frame
    void Update()
    {
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (Mathf.Abs(other.transform.position.z - transform.position.z) <= 2.2f && Mathf.Abs(other.transform.position.x - transform.position.x) <= 0.5f)
            {
                animator.Play("SwordAttack");
            }
            if (Mathf.Abs(other.transform.position.z - transform.position.z) < 0.5f && Mathf.Abs(other.transform.position.x - transform.position.x) <= 2.2f)
            {
                animator.Play("SwordAttack");
            }
        }
    }
}
