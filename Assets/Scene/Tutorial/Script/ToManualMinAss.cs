﻿using UnityEngine;
using System.Collections;

public class ToManualMinAss : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private bool isLoaded = false;

    public void SceneLoad()
    {
        isLoaded = !isLoaded;
        if (isLoaded)
        {
            Application.LoadLevelAdditive("ManualMinAssAttack");


        }
        else {
            Application.UnloadLevel("ManualMinAssAttack");
            Resources.UnloadUnusedAssets();
        }
    }
}

