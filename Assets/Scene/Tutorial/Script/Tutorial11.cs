﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Tutorial11 : MonoBehaviour
{

    public float time;
    public float count;

    // Use this for initialization
    void Start()
    {
        time = 0;
        count = 0;
    }

    // Update is called once per frame
    void Update()
    {
        time += 1 * Time.deltaTime;
        if (time >= 2)
        {
            if (GameObject.FindGameObjectsWithTag("Enemy").Length == 0)
            {
                count += Time.deltaTime;
                if (count >= 1.2)
                {
                    SceneManager.LoadScene("Tutorial12-1");
                }
            }
            if (GameObject.FindGameObjectsWithTag("Post").Length == 0)
            {
                count += Time.deltaTime;
                if (count >= 1.2)
                {
                    SceneManager.LoadScene("Tutorial12-2");
                }
            }
            if (GameObject.FindGameObjectsWithTag("Player").Length == 0 && GameObject.FindGameObjectsWithTag("Enemy").Length >= 1)
            {
                count += Time.deltaTime;
                if (count >= 1.2)
                {
                    SceneManager.LoadScene("Tutorial12-2");
                }
            }
        }
    }
}
