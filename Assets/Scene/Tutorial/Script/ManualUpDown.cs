﻿using UnityEngine;
using System.Collections;

public class ManualUpDown : MonoBehaviour
{

    public int a;
    public float time;
    public Vector3 ArrowPos;

    // Use this for initialization
    void Start()
    {
        time = -1f;
        a = 1;

    }

    // Update is called once per frame
    void Update()
    {
        if (time >= -1 && time <= 0)
        {
            transform.Translate(0, 0, 0.1f * Time.deltaTime);
        }

        time += 2f * Time.deltaTime;

        if (time >= 0 && a == 1)
        {
            ArrowPos = transform.position;
            a++;
        }
        if (time >= 2)
        {
            time = -1f;
            transform.position = new Vector3(5.12f, 0.025f, 8.3f);
            a = 1;
        }
    }
}
