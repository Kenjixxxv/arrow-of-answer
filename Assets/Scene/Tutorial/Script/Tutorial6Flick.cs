﻿using UnityEngine;
using System.Collections;

public class Tutorial6Flick : MonoBehaviour
{

    //スワイプ判定の最低距離 
    public float minSwipeDistX;
    public float minSwipeDistY;
    //実際にスワイプした距離 
    private float swipeDistX;
    private float swipeDistY;
    //方向判定に使うSign値 
    float SignValueX;
    float SignValueY;
    //タッチしたポジション 
    private Vector2 startPos;
    //タッチを離したポジション 
    private Vector2 endPos;
    public Vector3 ObjDist;
    //生成オブジェクト
    public GameObject PRight;
    public GameObject PUp;
    public GameObject SkipLogo;
    //タッチしたオブジェクト
    private GameObject Stage;
    public int rec;
    public GameObject FingerRight;
    public GameObject FingerUp;
    public GameObject SkipDammy;
    public GameObject LogoInst;


    public int a;


    void Start()
    {
        if (minSwipeDistX == 0)
        {
            minSwipeDistX = 10;
        }
        if (minSwipeDistY == 0)
        {
            minSwipeDistY = 10;
        }
        rec = 0;

        SkipDammy = GameObject.Find("SkipButtonDammy");
        LogoInst = GameObject.Find("LogoInst");
        a = 0;
    }


    void Update()
    {
        FingerRight = GameObject.Find("FingerRight(Clone)");
        FingerUp = GameObject.Find("FingerUp(Clone)");
        if(GameObject.FindGameObjectsWithTag("Dammy").Length == 0 && a == 0)
        {
            a = 1;
        }
        //タッチされたら 
        if (Input.touchCount > 0)
        {

            //タッチを取得 

            Touch touch = Input.touches[0];
            //タッチフェーズによって場合分け 
            switch (touch.phase)
            {

                //タッチ開始時 
                case TouchPhase.Began:

                    //タッチのポジションを取得してstartPosに代入 
                    startPos = touch.position;
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit = new RaycastHit();

                    if (Physics.Raycast(ray, out hit))
                    {
                        GameObject obj = hit.collider.gameObject;
                        Stage = hit.collider.gameObject;
                        ObjDist = obj.transform.position;
                        if (obj.name == "CubeRight")
                        {
                            rec = 1;
                        }
                        if (obj.name == "CubeUp")
                        {
                            rec = 1;
                        }

                    }
                    break;


                //タッチ終了時 
                case TouchPhase.Ended:


                    //タッチ終了のポジションをendPosに代入 
                    endPos = new Vector2(touch.position.x, touch.position.y);

                    //XY方向にスワイプした距離を算出 
                    swipeDistX = (new Vector3(endPos.x, 0, 0) - new Vector3(startPos.x, 0, 0)).magnitude;
                    swipeDistY = (new Vector3(0, endPos.y, 0) - new Vector3(0, startPos.y, 0)).magnitude;

                    if (swipeDistX > minSwipeDistX || swipeDistY > minSwipeDistY)
                    {
                        if (swipeDistX > swipeDistY)
                        {
                            SignValueX = Mathf.Sign(endPos.x - startPos.x);

                            if (SignValueX > 0)
                            {
                                if (rec == 1 && a == 1)
                                {
                                    Destroy(Stage);
                                    Destroy(FingerRight);
                                    Instantiate(PRight, ObjDist, PRight.transform.rotation);
                                    rec = 0;
                                    a = 2;
                                }

                            }
                        }

                    }
                    if (swipeDistY > swipeDistX)
                    {
                        SignValueY = Mathf.Sign(endPos.y - startPos.y);

                        if (SignValueY > 0)
                        {
                            if (rec == 1 && a == 2)
                            {
                                Destroy(Stage);
                                Destroy(FingerUp);
                                Destroy(SkipDammy);
                                Instantiate(PUp, ObjDist, PUp.transform.rotation);
                                Instantiate(SkipLogo, LogoInst.transform.position, LogoInst.transform.rotation);
                                rec = 0;

                            }
                            return;
                        }
                    }
                        break;
            }
        }
    }
}