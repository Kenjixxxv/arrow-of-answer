﻿using UnityEngine;
using System.Collections;

public class ToManual1Pre : MonoBehaviour {

    public GameObject ManualLogo;
    public GameObject ArrowRed;

    // Use this for initialization
    void Start () {
        ManualLogo = GameObject.Find("Manual");
        ArrowRed = GameObject.Find("ArrowRed");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private bool isLoaded = false;

    public void SceneLoad()
    {
        isLoaded = !isLoaded;
        if (isLoaded)
        {
           Application.LoadLevelAdditive("ManualPAttack");
            Destroy(ManualLogo);
           
        }
        else {
            Application.UnloadLevel("ManualPAttack");
            Destroy(ArrowRed);
            Resources.UnloadUnusedAssets();
        }
    }
}