﻿using UnityEngine;
using System.Collections;

public class PSwordAnim : MonoBehaviour
{
    Animator animator;

    // Use this for initialization
    void Start()
    {
        animator = GetComponent(typeof(Animator)) as Animator;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            animator.Play("SwordAttack");
            Debug.Log("PAV-");
        }
    }
}