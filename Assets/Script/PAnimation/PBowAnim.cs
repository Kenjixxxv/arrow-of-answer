﻿using UnityEngine;
using System.Collections;

public class PBowAnim : MonoBehaviour
{
    Animator animator;

    // Use this for initialization
    void Start()
    {
        animator = GetComponent(typeof(Animator)) as Animator;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            if (Mathf.Abs(other.transform.position.z - transform.position.z) < 2.2f && Mathf.Abs(other.transform.position.x - transform.position.x) < 1.2f)
            {
                animator.Play("BowRightAttack");
            }
        }
    }
}