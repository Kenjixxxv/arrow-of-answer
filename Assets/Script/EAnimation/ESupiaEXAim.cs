﻿using UnityEngine;
using System.Collections;

public class ESupiaEXAim : MonoBehaviour
{
    Animator animator;

    // Use this for initialization
    void Start()
    {
        animator = GetComponent(typeof(Animator)) as Animator;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (Mathf.Abs(other.transform.position.z - transform.position.z) <= 3.1f && Mathf.Abs(other.transform.position.x - transform.position.x) < 0.5f)
            {
                animator.Play("SupiaAttack");
                //Debug.Log("-PAS-");
            }
            if (Mathf.Abs(other.transform.position.z - transform.position.z) < 0.5f && Mathf.Abs(other.transform.position.x - transform.position.x) <= 3.1f)
            {
                animator.Play("SupiaAttack");
                //Debug.Log("-PAS-");
            }
        }
    }
}
