﻿using UnityEngine;
using System.Collections;

public class EAaasassinAnim : MonoBehaviour
{
    Animator animator;

    // Use this for initialization
    void Start()
    {
        animator = GetComponent(typeof(Animator)) as Animator;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            animator.Play("AssassinAttack");
            Debug.Log("-EAA-");
        }
    }
}