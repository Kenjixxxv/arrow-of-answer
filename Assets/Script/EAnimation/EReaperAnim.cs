﻿using UnityEngine;
using System.Collections;

public class EReaperAnim : MonoBehaviour
{
    Animator animator;

    // Use this for initialization
    void Start()
    {
        animator = GetComponent(typeof(Animator)) as Animator;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (Mathf.Abs(other.transform.position.z - transform.position.z) <= 2.1f && Mathf.Abs(other.transform.position.x - transform.position.x) <= 1.1f)
            {
                animator.Play("ReaperAttack");
                //Debug.Log("-EAR-");
            }
        }
    }
}