﻿using UnityEngine;
using System.Collections;

public class ESwordAnim : MonoBehaviour
{
    Animator animator;

    // Use this for initialization
    void Start()
    {
        animator = GetComponent(typeof(Animator)) as Animator;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            animator.Play("SwordAttack");
            Debug.Log("-EAV-");
        }
    }
}