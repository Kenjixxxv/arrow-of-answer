﻿using UnityEngine;
using System.Collections;

public class WinExplode : MonoBehaviour {

    public float time;
    public GameObject WinExplodePrefab;
	// Use this for initialization
	void Start () {
        time = 0;
	}
	
	// Update is called once per frame
	void Update () {
        time += 1 * Time.deltaTime;

        if(time >= 1.5f)
        {
            Instantiate(WinExplodePrefab, transform.position, transform.rotation);
        }
        if(time >= 1.8f)
        {
            Destroy(gameObject);
        }
	}
}
