﻿using UnityEngine;
using System.Collections;

public class Assassinanim : MonoBehaviour
{

    Animator animator;
    // Use this for initialization
    void Start()
    {
        animator = GetComponent(typeof(Animator)) as Animator;
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (Mathf.Abs(other.transform.position.z - transform.position.z) < 1.2f && Mathf.Abs(other.transform.position.x - transform.position.x) < 0.5f)
            {
                animator.Play("AssassinAttack");
            }
            if (Mathf.Abs(other.transform.position.z - transform.position.z) < 0.5f && Mathf.Abs(other.transform.position.x - transform.position.x) < 1.2f)
            {
                animator.Play("AssassinAttack");
            }

        }
    }
}
