﻿using UnityEngine;
using System.Collections;

public class PDefence : MonoBehaviour {

    public GameObject PDef;
    public GameObject Explode;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter(Collider other)
    {

        if (other.tag == "EAttack")
        {
                Instantiate(Explode, other.transform.position, other.transform.rotation);
                Destroy(gameObject,0.35f);
            
        }
        if (other.tag == "Defence")
        {
            Instantiate(PDef, transform.position, transform.rotation);
            Destroy(gameObject, 0.2f);
        }
    }
}
