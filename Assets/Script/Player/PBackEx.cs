﻿using UnityEngine;
using System.Collections;

public class PBackEx : MonoBehaviour {

    public float contact;

    public GameObject Explode;
    public GameObject Expand;

    // Use this for initialization
    void Start() {
        contact = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(contact >= 0.0001)
        {
            contact += 1 * Time.deltaTime;
        }
        if (contact > 0.15)
        {
            Instantiate(Expand, transform.position, transform.rotation);

        }
    }
    void OnTriggerEnter(Collider other)
    {

        if (other.tag == "EAttack")
        {
            Instantiate(Explode, other.transform.position, other.transform.rotation);
            Destroy(gameObject, 0.35f);
            contact += 1 * Time.deltaTime;
        }

    }
}
