﻿using UnityEngine;
using System.Collections;

public class DestroyStage : MonoBehaviour {

    public GameObject Cube;
    public GameObject Cube1;
    public GameObject CubeClone;
    public GameObject PDownCube;
    public GameObject PLeftCube;
    public GameObject PRightCube;
    public GameObject PUpCube;
    public GameObject ERight;
    public GameObject ELeft;
    public GameObject EUp;
    public GameObject EDown;
    public GameObject Expand;
    public GameObject Defence;



    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (GameObject.FindGameObjectsWithTag("PCube").Length >= 1)
        {
            GameObject[] a = GameObject.FindGameObjectsWithTag("PCube");
            foreach (GameObject obs in a)
            {
                Destroy(obs);
            }

        }
        Cube = GameObject.Find("Cube");
            Destroy(Cube);
        Cube1 = GameObject.Find("Cube1");
            Destroy(Cube1);
        CubeClone = GameObject.Find("Cube(Clone)");
            Destroy(CubeClone);
        PDownCube = GameObject.Find("PDownCube(Clone)");
            Destroy(PDownCube);
        PLeftCube = GameObject.Find("PLeftCube(Clone)");
            Destroy(PLeftCube);
        PRightCube = GameObject.Find("PRightCube(Clone)");
            Destroy(PRightCube);
        PUpCube = GameObject.Find("PUpCube(Clone)");
            Destroy(PUpCube);
        ERight = GameObject.Find("ERight");
            Destroy(ERight);
        ELeft = GameObject.Find("ELeft");
            Destroy(ELeft);
        EUp = GameObject.Find("EUp");
            Destroy(EUp);
        EDown = GameObject.Find("EDown");
            Destroy(EDown);
        Expand = GameObject.Find("Expand");
            Destroy(Expand);
        Defence = GameObject.Find("Defence");
            Destroy(Defence);
    }
}
