﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ScoreCount : MonoBehaviour {

    public int Score;
    public int count;

    public GameObject Dummy;
	// Use this for initialization
	void Start () {
        int SaveScore = PlayerPrefs.GetInt("SaveStage", -1);
        int SaveCount = PlayerPrefs.GetInt("Savenum", -1);
        Score = SaveScore;
        count = SaveCount;
	}
	
	// Update is called once per frame
	void Update () {
        if (Application.loadedLevelName == "Tutorial12-1" && count == -1)
        {
            count = 0;
            Score = 0;
            PlayerPrefs.SetInt("SaveStage", Score);
            PlayerPrefs.SetInt("Savenum", count);
            PlayerPrefs.Save();
        }
        if (Application.loadedLevelName == "Win1" && count == 0)
        {
                count = 1;
                Score = 1;
            PlayerPrefs.SetInt("SaveStage", Score);
            PlayerPrefs.SetInt("Savenum", count);
            PlayerPrefs.Save();
        }
            if (Application.loadedLevelName == "Win2" && count == 1)
            {
                count = 2;
                Score = 2;
            PlayerPrefs.SetInt("SaveStage", Score);
            PlayerPrefs.SetInt("Savenum", count);
            PlayerPrefs.Save();
        }
        
            if (Application.loadedLevelName == "Win3" && count == 2)
            {
                count = 3;
                Score = 3;
            PlayerPrefs.SetInt("SaveStage", Score);
            PlayerPrefs.SetInt("Savenum", count);
            PlayerPrefs.Save();
        }
        

            if (Application.loadedLevelName == "Win4" && count == 3)
            {
                count = 4;
                Score = 4;
            PlayerPrefs.SetInt("SaveStage", Score);
            PlayerPrefs.SetInt("Savenum", count);
            PlayerPrefs.Save();
        }
        

            if (Application.loadedLevelName == "Win5" && count == 4)
            {
                count = 5;
                Score = 5;
            PlayerPrefs.SetInt("SaveStage", Score);
            PlayerPrefs.SetInt("Savenum", count);
            PlayerPrefs.Save();
        }
        

            if (Application.loadedLevelName == "Win6" && count == 5)
            {
                count = 6;
                Score = 6;
            PlayerPrefs.SetInt("SaveStage", Score);
            PlayerPrefs.SetInt("Savenum", count);
            PlayerPrefs.Save();
        }
        

            if (Application.loadedLevelName == "Win7" && count == 6)
            {
                count = 7;
                Score = 7;
            PlayerPrefs.SetInt("SaveStage", Score);
            PlayerPrefs.SetInt("Savenum", count);
            PlayerPrefs.Save();
        }

            if (Application.loadedLevelName == "Win8" && count == 7)
            {
                count = 8;
                Score = 8;
            PlayerPrefs.SetInt("SaveStage", Score);
            PlayerPrefs.SetInt("Savenum", count);
            PlayerPrefs.Save();
        }

            if (Application.loadedLevelName == "Win9" && count == 8)
            {
                count = 9;
                Score = 9;
            PlayerPrefs.SetInt("SaveStage", Score);
            PlayerPrefs.SetInt("Savenum", count);
            PlayerPrefs.Save();
        }

            if (Application.loadedLevelName == "Win10" && count == 9)
            {
                count = 10;
                Score = 10;
            PlayerPrefs.SetInt("SaveStage", Score);
            PlayerPrefs.SetInt("Savenum", count);
            PlayerPrefs.Save();
        }

            if (Application.loadedLevelName == "Win11" && count == 10)
            {
                count = 11;
                Score = 11;
            PlayerPrefs.SetInt("SaveStage", Score);
            PlayerPrefs.SetInt("Savenum", count);
            PlayerPrefs.Save();
        }

            if (Application.loadedLevelName == "Win12" && count == 11)
            {
                count = 12;
                Score = 12;
            PlayerPrefs.SetInt("SaveStage", Score);
            PlayerPrefs.SetInt("Savenum", count);
            PlayerPrefs.Save();
        }

            if (Application.loadedLevelName == "Win13" && count == 12)
            {
                count = 13;
                Score = 13;
            PlayerPrefs.SetInt("SaveStage", Score);
            PlayerPrefs.SetInt("Savenum", count);
            PlayerPrefs.Save();
        }

            if (Application.loadedLevelName == "Win14" && count == 13)
            {
                count = 14;
                Score = 14;
            PlayerPrefs.SetInt("SaveStage", Score);
            PlayerPrefs.SetInt("Savenum", count);
            PlayerPrefs.Save();
        }

            if (Application.loadedLevelName == "Win15" && count == 14)
            {
                count = 15;
                Score = 15;
            PlayerPrefs.SetInt("SaveStage", Score);
            PlayerPrefs.SetInt("Savenum", count);
            PlayerPrefs.Save();
        }

            if (Application.loadedLevelName == "Win16" && count == 15)
           {
                count = 16;
                Score = 16;
            PlayerPrefs.SetInt("SaveStage", Score);
            PlayerPrefs.SetInt("Savenum", count);
            PlayerPrefs.Save();
        }
        if (Application.loadedLevelName == "Win17" && count == 16)
        {
            count = 17;
            Score = 17;
            PlayerPrefs.SetInt("SaveStage", Score);
            PlayerPrefs.SetInt("Savenum", count);
            PlayerPrefs.Save();
        }

        if (Application.loadedLevelName == "Win18" && count == 17)
        {
           count = 18;
           Score = 18;
           PlayerPrefs.SetInt("SaveStage", Score);
           PlayerPrefs.SetInt("Savenum", count);
           PlayerPrefs.Save();
        }

        if (Application.loadedLevelName == "Win19" && count == 18)
        {
            count = 19;
            Score = 19;
            PlayerPrefs.SetInt("SaveStage", Score);
            PlayerPrefs.SetInt("Savenum", count);
           PlayerPrefs.Save();
        }

        if (Application.loadedLevelName == "Win20" && count == 19)
        {
            count = 20;
            Score = 20;
            PlayerPrefs.SetInt("SaveStage", Score);
            PlayerPrefs.SetInt("Savenum", count);
            PlayerPrefs.Save();
        }

        if (Score >= 0)
        {
            Dummy = GameObject.Find("Dummy1");
            Destroy(Dummy);
        }
           if(Score >= 1)
        {
            Dummy = GameObject.Find("Dummy2");
            Destroy(Dummy);
        }
        if (Score >= 2)
        {
            Dummy = GameObject.Find("Dummy3");
            Destroy(Dummy);
        }
        if (Score >= 3)
        {
            Dummy = GameObject.Find("Dummy4");
            Destroy(Dummy);
        }
        if (Score >= 4)
        {
            Dummy = GameObject.Find("Dummy5");
            Destroy(Dummy);
        }
        if (Score >= 5)
        {
            Dummy = GameObject.Find("Dummy6");
            Destroy(Dummy);
        }
        if (Score >= 6)
        {
            Dummy = GameObject.Find("Dummy7");
            Destroy(Dummy);
        }
        if (Score >= 7)
        {
            Dummy = GameObject.Find("Dummy8");
            Destroy(Dummy);
        }
        if (Score >= 8)
        {
            Dummy = GameObject.Find("Dummy9");
            Destroy(Dummy);
        }
        if (Score >= 9)
        {
            Dummy = GameObject.Find("Dummy10");
            Destroy(Dummy);
        }
        if (Score >= 10)
        {
            Dummy = GameObject.Find("Dummy11");
            Destroy(Dummy);
        }
        if (Score >= 11)
        {
            Dummy = GameObject.Find("Dummy12");
            Destroy(Dummy);
        }
        if (Score >= 12)
        {
            Dummy = GameObject.Find("Dummy13");
            Destroy(Dummy);
        }
        if (Score >= 13)
        {
            Dummy = GameObject.Find("Dummy14");
            Destroy(Dummy);
        }
        if (Score >= 14)
        {
            Dummy = GameObject.Find("Dummy15");
            Destroy(Dummy);
        }
        if (Score >= 15)
        {
            Dummy = GameObject.Find("Dummy16");
            Destroy(Dummy);
        }
        if (Score >= 16)
        {
            Dummy = GameObject.Find("Dummy17");
            Destroy(Dummy);
        }
        if (Score >= 17)
        {
            Dummy = GameObject.Find("Dummy18");
            Destroy(Dummy);
        }
        if (Score >= 18)
        {
            Dummy = GameObject.Find("Dummy19");
            Destroy(Dummy);
        }
        if (Score >= 19)
        {
            Dummy = GameObject.Find("Dummy20");
            Destroy(Dummy);
        }

    }

    }

