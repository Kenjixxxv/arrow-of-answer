﻿using UnityEngine;
using System.Collections;

public class Dest5Sec : MonoBehaviour
{

    public float time;
    // Use this for initialization
    void Start()
    {
        time = 0;
    }

    // Update is called once per frame
    void Update()
    {
        time += 1 * Time.deltaTime;
        if (time >= 5)
        {
            Destroy(gameObject);
        }
    }
}
