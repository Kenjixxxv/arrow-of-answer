﻿using UnityEngine;
using System.Collections;

public class OtherDes : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    var PDown =  GameObject.Find("PDownCube/PDown");
        Destroy(PDown);
        var PUp = GameObject.Find("PUpCube/PUp");
        Destroy(PUp);
        var PRight = GameObject.Find("PRightCube/PRight");
        Destroy(PRight);
        var PLeft = GameObject.Find("PLeftCube/PLeft");
        Destroy(PLeft);
        var Defence = GameObject.Find("Defence");
        Destroy(Defence);
        var Expand = GameObject.Find("Expand");
        Destroy(Expand);
        var EDown = GameObject.Find("EDown");
        Destroy(EDown);
        var EUp = GameObject.Find("EUp");
        Destroy(EUp);
        var ERight = GameObject.Find("ERight");
        Destroy(ERight);
        var ELeft = GameObject.Find("ELeft");
        Destroy(ELeft);
        var Post = GameObject.Find("castle");
        Destroy(Post);
    }
}
