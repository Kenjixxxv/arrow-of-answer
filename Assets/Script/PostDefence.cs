﻿using UnityEngine;
using System.Collections;

public class PostDefence : MonoBehaviour {

    public GameObject Explode;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {

        if (other.tag == "EAttack")
        {
                Instantiate(Explode, other.transform.position, other.transform.rotation);
                Destroy(gameObject);
        }
    }
}
