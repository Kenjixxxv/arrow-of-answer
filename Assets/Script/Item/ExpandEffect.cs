﻿using UnityEngine;
using System.Collections;

public class ExpandEffect : MonoBehaviour {

    public GameObject ExpandPrefab;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Expand")
        {
            Instantiate(ExpandPrefab, transform.position, transform.rotation);
            Destroy(gameObject, 0.45f);
        }
    }
}
