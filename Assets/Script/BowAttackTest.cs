﻿using UnityEngine;
using System.Collections;

public class BowAttackTest : MonoBehaviour {

    Animator animator;

	// Use this for initialization
	void Start () {
        animator = GetComponent(typeof(Animator)) as Animator;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("space"))
        {
            animator.Play("BowAttack");
        }
	}
}
