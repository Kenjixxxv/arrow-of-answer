﻿using UnityEngine;
using System.Collections;

public class AMAXTrans : MonoBehaviour
{

    public float a, b;
    public GameObject ExplodePrehab;
    public Vector3 beforepos;
    public Vector3 afterpos;
    public Vector3 repf;
    public Vector3 reps;
    public int count;
    public int dist;
    public int rote;
    public float maxv, minv, maxh, minh;
    public int trans;
    public float time;
    public float contime;



    // Use this for initialization
    void Start()
    {
        a = 0;
        b = 0;
        count = 0;
        rote = 0;
        beforepos = transform.position;
        afterpos = transform.position;
        trans = 1;


    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.FindGameObjectsWithTag("Player").Length != 0)
        {
            transform.Translate(0, 0, 2 * Time.deltaTime);
            time += 2 * Time.deltaTime;
            if (trans == 2 && time - contime > 0.1)
            {
                trans = 1;
            }

        }
        if (GameObject.FindGameObjectsWithTag("Player").Length == 0)
        {
            transform.Translate(0, 0, 0);
        }
        if (GameObject.FindGameObjectsWithTag("Post").Length == 0)
        {
            transform.Translate(0, 0, 0);
        }
        if (transform.position.x <= 3 || transform.position.x >= 11)
        {
            Instantiate(ExplodePrehab, transform.position, transform.rotation);
            Destroy(gameObject);
        }
        if (transform.position.z <= 0 || transform.position.z >= 10)
        {
            Instantiate(ExplodePrehab, transform.position, transform.rotation);
            Destroy(gameObject);
        }

        beforepos = afterpos;
        afterpos = transform.position;

        if (count == 2)
        {
            if (repf.x > reps.x)
            {
                maxh = repf.x;
                minh = reps.x;
            }
            if (repf.x < reps.x)
            {
                maxh = reps.x;
                minh = repf.x;
            }
            if (repf.z > reps.z)
            {
                maxv = repf.z;
                minv = reps.z;
            }
            if (repf.z < reps.z)
            {
                maxv = reps.z;
                minv = repf.z;
            }

            if (dist == 1)
            {
                if (transform.position.x > maxh + 0.1f && rote == 1)
                {
                    transform.Rotate(0, 180, 0);
                    rote = 2;

                }
                if (transform.position.x < minh - 0.1f && rote == 2)
                {
                    transform.Rotate(0, 180, 0);
                    rote = 1;

                }

            }
            if (dist == 2)
            {
                if (transform.position.z > maxv +0.1f && rote == 1)
                {
                    transform.Rotate(0, 180, 0);
                    rote = 2;

                }
                if (transform.position.z < minv -0.1f && rote == 2)
                {
                    transform.Rotate(0, 180, 0);
                    rote = 1;

                }
            }

        }

    }
    void OnTriggerEnter(Collider other)
    {
        if (count == 2)
        {
            return;
        }

        if (other.tag == "EUp")
        {
            if (afterpos.x == beforepos.x)
            {
                if (afterpos.z < beforepos.z && trans == 1)
                {
                    transform.Rotate(0, 180, 0);
                    a = Mathf.Round(other.gameObject.transform.position.x);
                    b = Mathf.Round(other.gameObject.transform.position.z);
                    transform.position = new Vector3(a, afterpos.y, b);
                    count++;
                    trans++;
                    contime = time;
                    if (count == 1)
                    {
                        repf = new Vector3(a, afterpos.y, b);
                        dist = 2;
                    }
                    if (count == 2)
                    {
                        reps = new Vector3(a, afterpos.y, b);
                        rote = 1;
                    }
                    return;
                }
            }
            if (afterpos.z == beforepos.z)
            {
                if (afterpos.x > beforepos.x && trans == 1)
                {
                    transform.Rotate(0, -90, 0);
                    a = Mathf.Round(other.gameObject.transform.position.x);
                    b = Mathf.Round(other.gameObject.transform.position.z);
                    transform.position = new Vector3(a, afterpos.y, b);
                    trans++;
                    contime = time;
                    return;

                }
                if (afterpos.x < beforepos.x && trans == 1)
                {
                    transform.Rotate(0, 90, 0);
                    a = Mathf.Round(other.gameObject.transform.position.x);
                    b = Mathf.Round(other.gameObject.transform.position.z);
                    transform.position = new Vector3(a, afterpos.y, b);
                    trans++;
                    contime = time;
                    return;

                }
            }
        }


        if (other.tag == "ERight")
        {
            if (afterpos.x == beforepos.x)
            {
                if (afterpos.z > beforepos.z && trans == 1)
                {
                    transform.Rotate(0, 90, 0);
                    a = Mathf.Round(other.gameObject.transform.position.x);
                    b = Mathf.Round(other.gameObject.transform.position.z);
                    transform.position = new Vector3(a, afterpos.y, b);
                    trans++;
                    contime = time;
                    return;
                }
                if (afterpos.z < beforepos.z && trans == 1)
                {
                    transform.Rotate(0, -90, 0);
                    a = Mathf.Round(other.gameObject.transform.position.x);
                    b = Mathf.Round(other.gameObject.transform.position.z);
                    transform.position = new Vector3(a, afterpos.y, b);
                    trans++;
                    contime = time;
                    return;
                }
            }
            if (afterpos.z == beforepos.z)
            {
                if (afterpos.x < beforepos.x && trans == 1)
                {
                    transform.Rotate(0, 180, 0);
                    a = Mathf.Round(other.gameObject.transform.position.x);
                    b = Mathf.Round(other.gameObject.transform.position.z);
                    transform.position = new Vector3(a, afterpos.y, b);
                    count++;
                    trans++;
                    contime = time;
                    if (count == 1)
                    {
                        repf = new Vector3(a, afterpos.y, b);
                        dist = 1;
                    }
                    if (count == 2)
                    {
                        reps = new Vector3(a, afterpos.y, b);
                        rote = 1;
                    }
                    return;
                }


            }

        }
        if (other.tag == "EDown")
        {
            if (afterpos.z == beforepos.z)
            {
                if (afterpos.x > beforepos.x && trans == 1)
                {
                    transform.Rotate(0, 90, 0);
                    a = Mathf.Round(other.gameObject.transform.position.x);
                    b = Mathf.Round(other.gameObject.transform.position.z);
                    transform.position = new Vector3(a, afterpos.y, b);
                    trans++;
                    contime = time;
                    return;
                }
                if (afterpos.x < beforepos.x && trans == 1)
                {
                    transform.Rotate(0, -90, 0);
                    a = Mathf.Round(other.gameObject.transform.position.x);
                    b = Mathf.Round(other.gameObject.transform.position.z);
                    transform.position = new Vector3(a, afterpos.y, b);
                    trans++;
                    contime = time;
                    return;

                }
            }
            if (afterpos.x == beforepos.x)
            {
                if (afterpos.z > beforepos.z && trans == 1)
                {
                    transform.Rotate(0, 180, 0);
                    a = Mathf.Round(other.gameObject.transform.position.x);
                    b = Mathf.Round(other.gameObject.transform.position.z);
                    transform.position = new Vector3(a, afterpos.y, b);
                    trans++;
                    contime = time;
                    count++;
                    if (count == 1)
                    {
                        repf = new Vector3(a, afterpos.y, b);
                        dist = 2;
                    }
                    if (count == 2)
                    {
                        reps = new Vector3(a, afterpos.y, b);
                        rote = 2;
                    }
                    return;
                }
            }

        }
        if (other.tag == "ELeft")
        {
            if (afterpos.x == beforepos.x)
            {
                if (afterpos.z > beforepos.z && trans == 1)
                {
                    transform.Rotate(0, -90, 0);
                    a = Mathf.Round(other.gameObject.transform.position.x);
                    b = Mathf.Round(other.gameObject.transform.position.z);
                    transform.position = new Vector3(a, afterpos.y, b);
                    trans++;
                    contime = time;
                    return;
                }
                if (afterpos.z < beforepos.z && trans == 1)
                {
                    transform.Rotate(0, 90, 0);
                    a = Mathf.Round(other.gameObject.transform.position.x);
                    b = Mathf.Round(other.gameObject.transform.position.z);
                    transform.position = new Vector3(a, afterpos.y, b);
                    trans++;
                    contime = time;
                    return;

                }
            }
            if (afterpos.z == beforepos.z)
            {
                if (afterpos.x > beforepos.x && trans == 1)
                {
                    transform.Rotate(0, 180, 0);
                    a = Mathf.Round(other.gameObject.transform.position.x);
                    b = Mathf.Round(other.gameObject.transform.position.z);
                    transform.position = new Vector3(a, afterpos.y, b);
                    trans++;
                    contime = time;
                    count++;
                    if (count == 1)
                    {
                        repf = new Vector3(a, afterpos.y, b);
                        dist = 1;
                    }
                    if (count == 2)
                    {
                        reps = new Vector3(a, afterpos.y, b);
                        rote = 2;
                    }
                    return;
                }

            }
        }
    }
}
