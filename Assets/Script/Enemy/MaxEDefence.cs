﻿using UnityEngine;
using System.Collections;

public class MaxEDefence : MonoBehaviour
{

    public float count;
    public int add;
    public GameObject Explode;
    public GameObject stand;

    // Use this for initialization
    void Start()
    {
        count = 0;
        add = 0;
    }

    // Update is called once per frame
    void Update()
    {
        count += add * Time.deltaTime;
        if (count >= 0.2)
        {
            Instantiate(stand, transform.position, transform.rotation);
        }
    }
    void OnTriggerEnter(Collider other)
    {

        if (other.tag == "PAttack")
        {
            add = 1;
            Instantiate(Explode, other.transform.position, other.transform.rotation);
            Destroy(gameObject, 0.45f);
        }
    }
}

