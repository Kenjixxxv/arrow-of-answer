﻿using UnityEngine;
using System.Collections;

public class AssassinDefence : MonoBehaviour
{

    private int Life;
    public GameObject Explode;

    // Use this for initialization
    void Start()
    {
        Life = 1;
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter(Collider other)
    {

        if (other.tag == "PAttack")
        {
            Life--;
            if (Life == 0)
            {
                Instantiate(Explode, other.transform.position, other.transform.rotation);
                Destroy(gameObject);
            }
        }
        if (other.tag == "Defence")
        {
            Life++;
        }
    }
}