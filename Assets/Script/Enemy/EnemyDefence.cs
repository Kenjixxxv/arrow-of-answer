﻿using UnityEngine;
using System.Collections;

public class EnemyDefence : MonoBehaviour {

    public GameObject EDef;
    public GameObject Explode;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter(Collider other)
    {

        if (other.tag == "PAttack")
        {
            Instantiate(Explode, other.transform.position, other.transform.rotation);
            Destroy(gameObject, 0.35f);

        }
        if (other.tag == "Defence")
        {
            Instantiate(EDef, other.transform.position, transform.rotation);
            Destroy(gameObject, 0.2f);
        }
    }
}
