﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Select17 : MonoBehaviour
{
    public int a;
    // Use this for initialization
    void Start()
    {
        a = 1;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SceneLoad()
    {
        if (a == 1)
        {
            CameraFade.StartAlphaFade(Color.black, false, 0.5f, 0.5f, () => { SceneManager.LoadScene("Main17"); });
            a++;
        }
    }
}
