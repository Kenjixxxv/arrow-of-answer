﻿using UnityEngine;
using System.Collections;

public class Inst3Sec : MonoBehaviour
{

    public GameObject InstPrefab1;
    public float time;
    private int count;

    // Use this for initialization
    void Start()
    {
        time = 0;
        count = 1;

    }

    // Update is called once per frame
    void Update()
    {
        time += 1 * Time.deltaTime;
        if (time >= 3 && count == 1)
        {
            count--;
            Instantiate(InstPrefab1, transform.position, transform.rotation);
            Destroy(gameObject, 0.45f);
        }
    }
}