﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Skip11 : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SceneLoad()
    {
        CameraFade.StartAlphaFade(Color.black, false, 0.5f, 0.5f, () => { SceneManager.LoadScene("Stage11"); });
    }
}