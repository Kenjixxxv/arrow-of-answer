﻿using UnityEngine;
using System.Collections;

public class FingerUp : MonoBehaviour
{

    public int a;
    public float time;
    public Vector3 FingerPos;

    // Use this for initialization
    void Start()
    {
        time = -1f;
        a = 1;

    }

    // Update is called once per frame
    void Update()
    {
        if (time >= -1 && time <= -0.2)
        {
            transform.Translate(3 * Time.deltaTime, 0, 0);
        }

        time += 3 * Time.deltaTime;

        if (time >= -0.1 && a == 1)
        {
            FingerPos = transform.position;
            a++;
        }
        if (time >= 2)
        {
            time = -1f;
            transform.position = new Vector3(7.1f, 0.66f, 2.4f);
            a = 1;
        }
    }
}
