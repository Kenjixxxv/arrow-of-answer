﻿using UnityEngine;
using System.Collections;

public class FingerInst2 : MonoBehaviour
{
    public int a;
    public GameObject Finger;
    // Use this for initialization
    void Start()
    {
        a = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.FindGameObjectsWithTag("Dammy").Length == 0 && GameObject.FindGameObjectsWithTag("Finger").Length == 0 && a == 1)
        {
            Instantiate(Finger, transform.position, transform.rotation);
            a++;
            Destroy(gameObject, 0.1f);

        }
    }
}
