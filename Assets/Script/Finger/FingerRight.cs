﻿using UnityEngine;
using System.Collections;

public class FingerRight : MonoBehaviour
{

    public int a;
    public float time;
    public Vector3 FingerPos;

    // Use this for initialization
    void Start()
    {
        time = -1f;
        a = 1;

    }

    // Update is called once per frame
    void Update()
    {
        if (time >= -1 && time <= -0.2)
        {
            transform.Translate(0, -3 * Time.deltaTime, 0);
        }

        time += 3 * Time.deltaTime;

        if (time >= -0.1 && a == 1)
        {
            FingerPos = transform.position;
            a++;
        }
        if (time >= 2)
        {
            time = -1f;
            transform.position = new Vector3(5.7f, 0.91f, 2.7f);
            a = 1;
        }
    }
}