﻿using UnityEngine;
using System.Collections;

public class FingerLeft: MonoBehaviour {

    public int a;
    public float time;
    public Vector3 FingerPos;

	// Use this for initialization
	void Start () {
        time = -1f;
        a = 1;

	}
	
	// Update is called once per frame
	void Update () {
        if (time >= -1 && time <= 0)
        {
            transform.Translate(0, 3 * Time.deltaTime, 0);
        }

        time += 3 * Time.deltaTime;

        if (time >= 0 && a == 1)
        {
            FingerPos = transform.position;
            a++;
        }
        if (time >= 2)
        {
            time = -1f;
            transform.position = new Vector3(6.5f, 0.8f, 3.5f);
            a = 1;
        }
        }
	}

