﻿using UnityEngine;
using System.Collections;

public class FingerInst : MonoBehaviour {

    public int b = 1;
    public GameObject Finger;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	if(GameObject.FindGameObjectsWithTag("Dammy").Length == 0 && b == 1)
        {
            Instantiate(Finger, transform.position, transform.rotation);
            b++;
            Destroy(gameObject, 0.1f);
        }
	}
}
