﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class Limit11 : MonoBehaviour
{

    public int timeMin;
    public float a;
    public float timesecten;
    public float timesecone;
    public float Zero = 0;
    public Text timeMinGUI;
    public Text timesectenGUI;
    public Text timeseconeGUI;


    // Use this for initialization
    void Start()
    {
        timeMin = 3;
        timesecten = 0;
        timesecone = 1;
        timeMinGUI.text = timeMin.ToString();
        timesectenGUI.text = timesecten.ToString();
        timeseconeGUI.text = timesecone.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (timesecten <= 0 && timesecone <= 0.08)
        {
            timeMin--;
            timesecten = 5;
            timesecone = 9.9f;
        }
        if (timeMin >= 0 && timesecten >= 0 && timesecone >= 0.01)
        {
            timesecone -= 1 * Time.deltaTime;
            a = Mathf.Floor(timesecone);
            timeMinGUI.text = timeMin.ToString();
            timesectenGUI.text = timesecten.ToString();
            timeseconeGUI.text = a.ToString();
        }
        if (timesecten >= 1 && timesecone <= 0.05)
        {
            timesecten--;
            timesecone = 9.9f;
        }
        if (timeMin <= 0 && timesecten <= 0 && timesecone <= 0.1)
        {
            CameraFade.StartAlphaFade(Color.black, false, 0.1f, 0.1f, () => { SceneManager.LoadScene("Stage11"); });
        }
;

    }

}